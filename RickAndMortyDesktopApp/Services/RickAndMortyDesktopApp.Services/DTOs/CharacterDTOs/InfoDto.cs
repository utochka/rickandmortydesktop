﻿using RickAndMortyDesktopApp.Services.Interfaces.DTOs.CharacterDTOs;

namespace RickAndMortyDesktopApp.Services.DTOs.CharacterDTOs
{
    public class InfoDto : IInfoDto
    {
        public int Count { get; set; }
        public int Pages { get; set; }
        public string Next { get; set; }
        public string Prev { get; set; }
    }
}
