﻿using System.Collections.Generic;

namespace RickAndMortyDesktopApp.Services.Interfaces.DTOs.CharacterDTOs
{
    public interface ICharacterListDto
    {
        IInfoDto Info { get; }
        IList<ICharacterDto> Results { get; }
    }
}
