﻿using RickAndMortyDesktopApp.Services.Interfaces.DTOs.CharacterDTOs;

namespace RickAndMortyDesktopApp.Services.DTOs.CharacterDTOs
{
    public class CharacterLocationDto : ICharacterLocationDto
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
