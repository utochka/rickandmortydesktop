﻿using RickAndMortyDesktopApp.Services.Interfaces.DTOs.CharacterDTOs;

namespace RickAndMortyDesktopApp.Services.DTOs.CharacterDTOs
{
    public class OriginDto : IOriginDto
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
