﻿using SharpVectors.Converters;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows;
using System.IO;

namespace RickAndMortyDesktopApp.Core.Shared
{
    public class SvgImageViewer : Grid
    {
        protected const string DefaultDirectoryPath = "/Resources/";

        public static readonly DependencyProperty DirectoryPathProperty
            = DependencyProperty.Register(
                nameof(DirectoryPath),
                typeof(string),
                typeof(SvgImageViewer),
                new PropertyMetadata(DefaultDirectoryPath, SourceChanged));


        public static readonly DependencyProperty SourceProperty
            = DependencyProperty.Register(
                nameof(Source),
                typeof(string),
                typeof(SvgImageViewer),
                new PropertyMetadata(null, SourceChanged));

        private static void SourceChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            var svgImageViewer = dependencyObject as SvgImageViewer;
            var directoryPath = svgImageViewer.DirectoryPath;
            var source = svgImageViewer.Source;

            if (string.IsNullOrWhiteSpace(directoryPath) || string.IsNullOrWhiteSpace(source))
            {
                return;
            }

            svgImageViewer.Children.Clear();
            svgImageViewer.ImageSource = Path.Combine(directoryPath, source);

            var image = new Image();
            var binding = new Binding(nameof(ImageSource))
            {
                Source = svgImageViewer,
                Converter = new SvgImageConverterExtension()
            };
            image.SetBinding(Image.SourceProperty, binding);

            svgImageViewer.Children.Add(image);
        }

        public string DirectoryPath
        {
            get => (string)GetValue(DirectoryPathProperty);
            set => SetValue(DirectoryPathProperty, value);
        }

        public string Source
        {
            get => (string)GetValue(SourceProperty);
            set => SetValue(SourceProperty, value);
        }

        public string ImageSource { get; set; }
    }
}
