﻿using RickAndMortyDesktopApp.Services.Interfaces.DTOs.CharacterDTOs;
using RickAndMortyDesktopApp.Services.Interfaces.DTOs.EpisodeDTOs;
using RickAndMortyDesktopApp.Services.Interfaces.DTOs.LocationDTOs;
using RickAndMortyDesktopApp.Services.Interfaces.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RickAndMortyDesktopApp.Services.Interfaces
{
    public interface IRickAndMortyApi
    {
        int CountCharactersPages { get; }

        Task<IList<IEpisodeDto>> GetEpisodesAsync();
        Task<IList<ILocationDto>> GetLocationsAsync();

        Task<IList<ICharacterModel>> GetCharactersModelAsync(int pagesCount);
        Task<IList<ICharacterModel>> GetAllPagesCharactersModelAsync(int skipPages);

        Task<IList<IEpisodeModel>> GetEpisodesModelsAsync();
        Task<IList<ILocationModel>> GetLocationsModelsAsync();
    }
}
