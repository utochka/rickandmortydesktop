﻿using Prism.Events;

namespace RickAndMortyDesktopApp.Core.Views.EpisodesView
{
    public class SelectedEpisodeEvent
        : PubSubEvent<SelectedEpisodeEventArgs>
    {
    }
}
