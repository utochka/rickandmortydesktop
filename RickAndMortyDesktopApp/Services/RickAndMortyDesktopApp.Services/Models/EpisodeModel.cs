﻿using RickAndMortyDesktopApp.Services.Interfaces.Models;
using System.Collections.Generic;

namespace RickAndMortyDesktopApp.Services.Models
{
    public class EpisodeModel : IEpisodeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string AirDate { get; set; }
        public string EpisodeCode { get; set; }
        public IList<string> Characters { get; set; }
        public IList<string> CharacterUrls { get; set; }
    }
}
