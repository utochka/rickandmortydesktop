﻿using Prism.Events;

namespace RickAndMortyDesktopApp.Core.Views.CharactersView
{
    public class SelectedCharacterEvent
        : PubSubEvent<SelectedCharacterEventArgs>
    {
    }
}
