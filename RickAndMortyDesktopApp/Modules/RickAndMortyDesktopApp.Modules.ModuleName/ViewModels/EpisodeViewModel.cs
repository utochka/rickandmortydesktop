﻿using Prism.Commands;
using Prism.Events;
using RickAndMortyDesktopApp.Core.Mvvm;
using RickAndMortyDesktopApp.Core.Views.EpisodesView;
using RickAndMortyDesktopApp.Services.Interfaces.Models;
using System.Collections.Generic;
using System.Linq;

namespace RickAndMortyDesktopApp.Modules.ModuleName.ViewModels
{
    public class EpisodeViewModel
        : ViewModelBase
    {
        #region Properties

        private readonly IEpisodeModel _episodeModel;
        private readonly IEventAggregator _eventAggregator;

        public int Id => _episodeModel.Id;
        public string Name => GetNonEmptyString(_episodeModel.Name.ToUpper());
        public string AirDate => GetNonEmptyString(_episodeModel.AirDate);
        public string EpisodeCode => GetNonEmptyString(_episodeModel.EpisodeCode);
        public IList<string> Characters => _episodeModel.Characters
                .Select(character => GetNonEmptyString(character))
                .ToList();
        public IList<string> CharacterUrls => _episodeModel.CharacterUrls
                .Select(character => GetNonEmptyString(character))
                .ToList();


        #endregion

        #region Initialize

        public EpisodeViewModel(
            IEpisodeModel model,
            IEventAggregator eventAggregator
            )
        {
            _episodeModel = model;
            _eventAggregator = eventAggregator;

            DetailsCommand = new DelegateCommand(OnDetails);
        }

        #endregion

        #region Commands

        public DelegateCommand DetailsCommand { get; set; }

        public void OnDetails()
        {
            _eventAggregator
                .GetEvent<SelectedEpisodeEvent>()
                .Publish(new SelectedEpisodeEventArgs(Id));
        }

        #endregion

        #region Methods

        private static string GetNonEmptyString(string value)
        {
            return string.IsNullOrEmpty(value) ? "-" : value;
        }

        #endregion
    }
}
