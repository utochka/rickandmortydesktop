﻿namespace RickAndMortyDesktopApp.Services.Interfaces.DTOs.LocationDTOs
{
    public interface ILocationInfoDto
    {
        int Count { get; }
        int Pages { get; }
        string Next { get; }
        string Prev { get; }
    }
}
