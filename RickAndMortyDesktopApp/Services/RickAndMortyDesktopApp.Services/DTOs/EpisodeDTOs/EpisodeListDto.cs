﻿using RickAndMortyDesktopApp.Services.Interfaces.DTOs.EpisodeDTOs;
using System.Collections.Generic;
using System.Linq;

namespace RickAndMortyDesktopApp.Services.DTOs.EpisodeDTOs
{
    public class EpisodeListDto : IEpisodeListDto
    {
        public EpisodeInfoDto Info { get; set; }
        public IList<EpisodeDto> Results { get; set; }
        IEpisodeInfoDto IEpisodeListDto.Info { get => Info; }
        IList<IEpisodeDto> IEpisodeListDto.Results
        {
            get => Results.Cast<IEpisodeDto>().ToList();
        }
    }
}
