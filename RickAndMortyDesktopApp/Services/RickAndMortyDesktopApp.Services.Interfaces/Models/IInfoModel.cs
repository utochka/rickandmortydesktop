﻿namespace RickAndMortyDesktopApp.Services.Interfaces.Models
{
    public interface IInfoModel
    {
        int Count { get; }
        int Pages { get; }
        string Next { get; }
        string Prev { get; }
    }
}
