﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using RickAndMortyDesktopApp.Core;
using RickAndMortyDesktopApp.Core.Views.Blackout;
using RickAndMortyDesktopApp.Core.Views.Errors;
using System.Windows;

namespace RickAndMortyDesktopApp.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private readonly IRegionManager _regionManager;
        private readonly IEventAggregator _eventAggregator;

        public MainWindowViewModel(
            IRegionManager regionManager,
            IEventAggregator eventAggregator
            )
        {
            _regionManager = regionManager;
            _eventAggregator = eventAggregator;

            _eventAggregator
                .GetEvent<BlackoutEvent>()
                .Subscribe(OnBlackout);

            _eventAggregator
                .GetEvent<ErrorEvent>()
                .Subscribe(OnError);

            CharactersCommand = new DelegateCommand(OnCharacters);
            EpisodesCommand = new DelegateCommand(OnEpisodes);
            LocationsCommand = new DelegateCommand(OnLocations);

            Initialize();
        }

        private void OnError()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                IsActiveEpisodesTab = false;
                IsActiveCharactersTab = false;
                IsActiveLocationsTab = false;

                _regionManager.RequestNavigate(RegionNames.ContentRegion, "ErrorView");
            });
        }

        private void OnBlackout(BlackoutEventArgs args)
        {
            BlackoutVisibility = args.IsVisible ? Visibility.Visible : Visibility.Collapsed;
        }

        private void Initialize()
        {
            OnCharacters();
        }

        #region Properties

        private bool _isActiveCharactersTab;
        public bool IsActiveCharactersTab
        {
            get => _isActiveCharactersTab;
            set => SetProperty(ref _isActiveCharactersTab, value);
        }

        private bool _isActiveEpisodesTab;
        public bool IsActiveEpisodesTab
        {
            get => _isActiveEpisodesTab;
            set => SetProperty(ref _isActiveEpisodesTab, value);
        }

        private bool _isActiveLocationsTab;
        public bool IsActiveLocationsTab
        {
            get => _isActiveLocationsTab;
            set => SetProperty(ref _isActiveLocationsTab, value);
        }

        private Visibility _blackoutVisibility = Visibility.Collapsed;

        public Visibility BlackoutVisibility
        {
            get => _blackoutVisibility;
            set => SetProperty(ref _blackoutVisibility, value);
        }

        #endregion

        #region Commands

        public DelegateCommand CharactersCommand { get; private set; }
        private void OnCharacters()
        {
            IsActiveCharactersTab = true;
            IsActiveEpisodesTab = false;
            IsActiveLocationsTab = false;
            _regionManager.RequestNavigate(RegionNames.ContentRegion, "CharactersView");
        }

        public DelegateCommand EpisodesCommand { get; private set; }
        private void OnEpisodes()
        {
            IsActiveEpisodesTab = true;
            IsActiveCharactersTab = false;
            IsActiveLocationsTab = false;
            _regionManager.RequestNavigate(RegionNames.ContentRegion, "EpisodesView");
        }

        public DelegateCommand LocationsCommand { get; private set; }
        private void OnLocations()
        {
            IsActiveLocationsTab = true;
            IsActiveCharactersTab = false;
            IsActiveEpisodesTab = false;
            _regionManager.RequestNavigate(RegionNames.ContentRegion, "LocationsView");
        }

        #endregion
    }
}
