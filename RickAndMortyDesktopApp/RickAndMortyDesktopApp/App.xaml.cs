﻿using Prism.Events;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;
using RickAndMortyDesktopApp.Core.Shared;
using RickAndMortyDesktopApp.Modules.ModuleName;
using RickAndMortyDesktopApp.Services;
using RickAndMortyDesktopApp.Services.Interfaces;
using RickAndMortyDesktopApp.Views;
using SharpVectors.Dom;
using System.Windows;

namespace RickAndMortyDesktopApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override Window CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterSingleton<IRickAndMortyApi, RickAndMortyApi>();

            var eventAggregator = Container.Resolve<IEventAggregator>();
            ExceptionHandler.Instance.Initialize(eventAggregator);
        }

        protected override void ConfigureModuleCatalog(IModuleCatalog moduleCatalog)
        {
            moduleCatalog.AddModule<ModuleNameModule>();
        }
    }
}
