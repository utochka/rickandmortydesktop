﻿using System.Collections.Generic;

namespace RickAndMortyDesktopApp.Services.Interfaces.Models
{
    public interface ILocationModel
    {
        int Id { get; }
        string Name { get; }
        string Type { get; }
        string Dimension { get; }
        IList<string> Residents { get; }
        IList<string> ResidentUrls { get; }
    }
}
