﻿namespace RickAndMortyDesktopApp.Services.Interfaces.Models
{
    public interface IOriginModel
    {
        string Name { get; }
        string Url { get; }
    }
}
