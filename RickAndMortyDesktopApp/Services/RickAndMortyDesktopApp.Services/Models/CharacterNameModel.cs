﻿namespace RickAndMortyDesktopApp.Services.Models
{
    public class CharacterNameModel
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
