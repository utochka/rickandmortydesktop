﻿using Prism.Commands;
using Prism.Events;
using RickAndMortyDesktopApp.Core.Mvvm;
using RickAndMortyDesktopApp.Core.Views.Blackout;
using RickAndMortyDesktopApp.Core.Views.CharactersView;
using RickAndMortyDesktopApp.Services.Interfaces;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace RickAndMortyDesktopApp.Modules.ModuleName.ViewModels
{
    public class CharactersViewModel
        : ViewModelBase
    {
        #region Properties

        private IRickAndMortyApi _rickAndMortyApi;
        private List<CharacterViewModel> _models;
        private readonly IEventAggregator _eventAggregator;

        private int skip = 0;
        private const int take = 4;

        private CharacterViewModel _selectedItem;
        public CharacterViewModel SelectedItem
        {
            get => _selectedItem;
            set => SetProperty(ref _selectedItem, value);
        }

        #endregion

        #region Initialize

        public CharactersViewModel(
            IRickAndMortyApi rickAndMortyApi,
            IEventAggregator eventAggregator
            )
        {
            _rickAndMortyApi = rickAndMortyApi;
            _eventAggregator = eventAggregator;

            _eventAggregator
               .GetEvent<SelectedCharacterEvent>()
               .Subscribe(OnSelectedCharacter);

            NextCommand = new DelegateCommand(OnNext, CanExecuteNext);
            PreviousCommand = new DelegateCommand(OnPrevious, CanExecuteNext);
            ClosePopupCommand = new DelegateCommand(OnClosePopup);

            Task.Run(InitializeAsync);
        }

        private async Task InitializeAsync()
        {
            var models = await _rickAndMortyApi.GetCharactersModelAsync(3);

            _models = models
                .Select(a => new CharacterViewModel(a, _eventAggregator))
                .ToList();

            List = new ObservableCollection<CharacterViewModel>(_models.Skip(skip).Take(take));

            Task.Run(LoadAllPagesAsync);
        }

        #endregion

        #region Events

        private void OnSelectedCharacter(SelectedCharacterEventArgs args)
        {
            SelectedItem = _models.Single(el => el.Id == args.Id);
            IsOpenPopup = true;

            _eventAggregator
                .GetEvent<BlackoutEvent>()
                .Publish(new BlackoutEventArgs(true));
        }

        public void OnClosePopup()
        {
            IsOpenPopup = false;
            SelectedItem = null;

            _eventAggregator
                .GetEvent<BlackoutEvent>()
                .Publish(new BlackoutEventArgs(false));
        }

        #endregion

        #region Commands


        private ObservableCollection<CharacterViewModel> _list;
        public ObservableCollection<CharacterViewModel> List
        {
            get => _list;
            set
            {
                if (SetProperty(ref _list, value))
                {
                    NextCommand.RaiseCanExecuteChanged();
                    PreviousCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public DelegateCommand PreviousCommand { set; get; }
        private void OnPrevious()
        {
            skip -= take;
            if (skip < 0)
            {
                skip = 0;
            }
            if (skip > _models.Count)
            {
                return;
            }

            List = new ObservableCollection<CharacterViewModel>(_models.Skip(skip).Take(take));
        }

        public DelegateCommand NextCommand { set; get; }
        private void OnNext()
        {
            skip += take;
            if (skip > _models.Count)
            {
                skip = _models.Count;
                return;
            }

            List = new ObservableCollection<CharacterViewModel>(_models.Skip(skip).Take(take));
        }

        private bool CanExecuteNext()
        {
            if (_models == null)
            {
                return false;
            }
            return skip <= _models.Count;
        }

        public DelegateCommand ClosePopupCommand { set; get; }

        private bool _isOpenPopup;
        public bool IsOpenPopup
        {
            get => _isOpenPopup;
            set => SetProperty(ref _isOpenPopup, value);
        }

        #endregion

        #region Methods

        private async Task LoadAllPagesAsync()
        {
            var models = await _rickAndMortyApi.GetAllPagesCharactersModelAsync(3);


            var viewModels = models
                .Select(a => new CharacterViewModel(a, _eventAggregator))
                .ToList();

            _models.AddRange(viewModels);
        }

        #endregion
    }
}
