﻿namespace RickAndMortyDesktopApp.Services.Interfaces.DTOs.CharacterDTOs
{
    public interface IInfoDto
    {
        int Count { get; }
        int Pages { get; }
        string Next { get; }
        string Prev { get; }
    }
}
