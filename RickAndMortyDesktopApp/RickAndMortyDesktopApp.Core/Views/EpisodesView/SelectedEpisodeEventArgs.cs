﻿using System;

namespace RickAndMortyDesktopApp.Core.Views.EpisodesView
{
    public class SelectedEpisodeEventArgs : EventArgs
    {
        public int Id { get; private set; }
        public SelectedEpisodeEventArgs(int id)
        {
            Id = id;
        }
    }
}

