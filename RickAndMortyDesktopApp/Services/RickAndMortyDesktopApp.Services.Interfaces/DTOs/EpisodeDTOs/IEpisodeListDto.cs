﻿using System.Collections.Generic;

namespace RickAndMortyDesktopApp.Services.Interfaces.DTOs.EpisodeDTOs
{
    public interface IEpisodeListDto
    {
        IEpisodeInfoDto Info { get;  }
        IList<IEpisodeDto> Results { get; }
    }
}
