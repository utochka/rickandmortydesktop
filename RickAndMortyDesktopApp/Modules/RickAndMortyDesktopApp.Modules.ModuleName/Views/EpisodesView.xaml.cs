﻿using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace RickAndMortyDesktopApp.Modules.ModuleName.Views
{
    public partial class EpisodesView : UserControl
    {
        public EpisodesView()
        {
            InitializeComponent();
        }

        private void HyperlinkRequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri) { UseShellExecute = true });

            e.Handled = true;
        }
    }
}
