﻿using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace RickAndMortyDesktopApp.Modules.ModuleName.Views
{
    public partial class CharactersView : UserControl
    {
        public CharactersView()
        {
            InitializeComponent();
        }

        private void HyperlinkRequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri) { UseShellExecute = true });

            e.Handled = true;
        }
    }
}
