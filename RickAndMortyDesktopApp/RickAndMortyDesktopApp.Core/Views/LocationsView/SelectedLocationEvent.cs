﻿using Prism.Events;

namespace RickAndMortyDesktopApp.Core.Views.LocationsView
{
    public class SelectedLocationEvent 
        : PubSubEvent<SelectedLocationEventArgs>
    {

    }
}
