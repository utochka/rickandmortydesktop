﻿using System.Threading.Tasks;
using System.Windows.Threading;
using System;
using Prism.Regions;
using Prism.Events;
using RickAndMortyDesktopApp.Core.Views.Blackout;
using RickAndMortyDesktopApp.Core.Views.Errors;

namespace RickAndMortyDesktopApp.Core.Shared
{
    public class ExceptionHandler
    {
        private IEventAggregator _eventAggregator;

        private static ExceptionHandler _exceptionHandler = new ExceptionHandler();
        public static ExceptionHandler Instance => _exceptionHandler;

        private ExceptionHandler()
        {
        }

        public void Initialize(Prism.Events.IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;

            SubscribeEvents();
        }

        private void SubscribeEvents()
        {
            AppDomain.CurrentDomain.UnhandledException += OnUnhandledException;
            System.Windows.Application.Current.DispatcherUnhandledException += OnDispatcherUnhandledException;
            Dispatcher.CurrentDispatcher.UnhandledException += OnUnhandledException;
            TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;
        }

        private void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            e.SetObserved();
            OnUnhandledException(e.Exception, nameof(TaskScheduler.UnobservedTaskException));
        }

        private void OnUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = OnUnhandledException(e.Exception, nameof(Dispatcher.CurrentDispatcher.UnhandledException));
        }

        private void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            OnUnhandledException((Exception)e.ExceptionObject, nameof(AppDomain.CurrentDomain.UnhandledException));
        }

        private void OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = OnUnhandledException(e.Exception, nameof(System.Windows.Application.Current.DispatcherUnhandledException));
        }

        public void UnhandledException(Exception exception)
        {
            OnUnhandledException(exception, nameof(ExceptionHandler.UnhandledException));
        }

        private bool OnUnhandledException(Exception exception, string v)
        {
            _eventAggregator
                .GetEvent<ErrorEvent>()
                .Publish();

            return true;
        }
    }
}
