﻿using RickAndMortyDesktopApp.Services.Interfaces.DTOs.LocationDTOs;
using System.Collections.Generic;
using System.Linq;

namespace RickAndMortyDesktopApp.Services.DTOs.LocationDTOs
{
    public class LocationListDto : ILocationListDto
    {
        public LocationInfoDto Info { get; set; }
        public IList<LocationDto> Results { get; set; }

        ILocationInfoDto ILocationListDto.Info { get => Info; }

        IList<ILocationDto> ILocationListDto.Results
        {
            get => Results.Cast<ILocationDto>().ToList();
        }
    }
}
