﻿using Prism.Events;
using System;

namespace RickAndMortyDesktopApp.Core.Views.Blackout
{
    public class BlackoutEvent : PubSubEvent<BlackoutEventArgs>
    {
    }

    public class BlackoutEventArgs : EventArgs
    {
        public bool IsVisible { get; private set; }

        public BlackoutEventArgs(bool isVisible)
        {
            IsVisible = isVisible;
        }
    }
}
