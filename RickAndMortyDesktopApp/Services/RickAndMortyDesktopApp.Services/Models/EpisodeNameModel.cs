﻿using RickAndMortyDesktopApp.Services.Interfaces.Models;

namespace RickAndMortyDesktopApp.Services.Models
{
    public class EpisodeNameModel
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
