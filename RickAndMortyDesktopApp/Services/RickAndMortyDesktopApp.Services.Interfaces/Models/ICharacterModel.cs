﻿using System.Collections.Generic;

namespace RickAndMortyDesktopApp.Services.Interfaces.Models
{
    public interface ICharacterModel
    {
        int Id { get; }
        string Name { get; }
        string Species { get; }
        string Status { get; }
        string Type { get; }
        string Gender { get; }
        IOriginModel Origin { get; }
        ICharacterLocationModel Location { get; }
        IList<ICharacterEpisodeModel> Episode { get; }
        string Image { get; }
    }
}
