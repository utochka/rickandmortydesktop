﻿using System.Collections.Generic;

namespace RickAndMortyDesktopApp.Services.Interfaces.Models
{
    public interface IEpisodeModel
    {
        int Id { get; }
        string Name { get; }
        string AirDate { get; }
        string EpisodeCode { get; }
        IList<string> Characters { get; }
        IList<string> CharacterUrls { get; }
    }
}
