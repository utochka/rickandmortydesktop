﻿using RickAndMortyDesktopApp.Services.Interfaces.DTOs.CharacterDTOs;
using System.Collections.Generic;
using System.Linq;

namespace RickAndMortyDesktopApp.Services.DTOs.CharacterDTOs
{
    public class CharacterListDto : ICharacterListDto
    {
        public InfoDto Info { get; set; }
        public List<CharacterDto> Results { get; set; }
        IInfoDto ICharacterListDto.Info { get => Info; }
        IList<ICharacterDto> ICharacterListDto.Results
        {
            get => Results.Cast<ICharacterDto>().ToList();
        }
    }
}
