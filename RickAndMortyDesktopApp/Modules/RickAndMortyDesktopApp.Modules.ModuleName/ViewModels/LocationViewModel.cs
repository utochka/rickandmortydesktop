﻿using Prism.Commands;
using Prism.Events;
using RickAndMortyDesktopApp.Core.Mvvm;
using RickAndMortyDesktopApp.Core.Views.LocationsView;
using RickAndMortyDesktopApp.Services.Interfaces.Models;
using System.Collections.Generic;
using System.Linq;

namespace RickAndMortyDesktopApp.Modules.ModuleName.ViewModels
{
    public class LocationViewModel
        : ViewModelBase
    {
        #region Properties

        private readonly ILocationModel _locationModel;
        private readonly IEventAggregator _eventAggregator;

        public int Id => _locationModel.Id;
        public string Name => GetNonEmptyString(_locationModel.Name);
        public string Type => GetNonEmptyString(_locationModel.Type);
        public string Dimension => GetNonEmptyString(_locationModel.Dimension);
        public IList<string> Residents => _locationModel.Residents
            .Select(location => GetNonEmptyString(location))
            .ToList();
        public IList<string> ResidentUrls => _locationModel.ResidentUrls
            .Select(location => GetNonEmptyString(location))
            .ToList();

        #endregion

        #region Initialize

        public LocationViewModel(ILocationModel model, 
            IEventAggregator eventAggregator
            )
        {
            _locationModel = model;
            _eventAggregator = eventAggregator;
            DetailsCommand = new DelegateCommand(OnDetails);
        }

        #endregion

        #region Commands

        public DelegateCommand DetailsCommand { get; set; }

        public void OnDetails()
        {
            _eventAggregator
                .GetEvent<SelectedLocationEvent>()
                .Publish(new SelectedLocationEventArgs(Id));
        }

        #endregion

        #region Methods

        private static string GetNonEmptyString(string value)
        {
            return string.IsNullOrEmpty(value) ? "-" : value;
        }

        #endregion
    }
}
