﻿using System;

namespace RickAndMortyDesktopApp.Core.Views.CharactersView
{
    public class SelectedCharacterEventArgs : EventArgs
    {
        public int Id { get; private set; }
        public SelectedCharacterEventArgs(int id)
        {
            Id = id;
        }
    }
}
