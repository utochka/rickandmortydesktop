﻿namespace RickAndMortyDesktopApp.Services.Interfaces.Models
{
    public interface ICharacterEpisodeModel
    {
        string Name { get; }
        string Url { get; }
    }
}