﻿using System;

namespace RickAndMortyDesktopApp.Core.Views.LocationsView
{
    public class SelectedLocationEventArgs : EventArgs
    {
        public int Id { get; private set; }

        public SelectedLocationEventArgs(int id)
        {
            Id = id;
        }
    }
}
