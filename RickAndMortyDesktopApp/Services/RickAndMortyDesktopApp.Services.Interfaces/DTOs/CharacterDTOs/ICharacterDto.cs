﻿using System;
using System.Collections.Generic;

namespace RickAndMortyDesktopApp.Services.Interfaces.DTOs.CharacterDTOs
{
    public interface ICharacterDto
    {
        int Id { get; }
        string Name { get; }
        string Status { get; }
        string Species { get; }
        string Type { get; }
        string Gender { get; }
        IOriginDto Origin { get; }
        ICharacterLocationDto Location { get; }
        string Image { get; }
        IList<string> Episode { get; }
        string Url { get; }
        DateTime Created { get; }
    }
}
