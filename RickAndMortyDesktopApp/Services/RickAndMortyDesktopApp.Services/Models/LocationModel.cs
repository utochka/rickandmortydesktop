﻿using RickAndMortyDesktopApp.Services.Interfaces.Models;
using System.Collections.Generic;

namespace RickAndMortyDesktopApp.Services.Models
{
    public class LocationModel : ILocationModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Dimension { get; set; }
        public IList<string> Residents { get; set; }
        public IList<string> ResidentUrls { get; set; }
    }
}
