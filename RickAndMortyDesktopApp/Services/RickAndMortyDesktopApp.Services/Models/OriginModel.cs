﻿using RickAndMortyDesktopApp.Services.Interfaces.Models;

namespace RickAndMortyDesktopApp.Services.Models
{
    public class OriginModel : IOriginModel
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
