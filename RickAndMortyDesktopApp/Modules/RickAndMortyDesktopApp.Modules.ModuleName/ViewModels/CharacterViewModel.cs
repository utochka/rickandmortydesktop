﻿using Prism.Commands;
using Prism.Events;
using RickAndMortyDesktopApp.Core.Mvvm;
using RickAndMortyDesktopApp.Core.Views.CharactersView;
using RickAndMortyDesktopApp.Services.Interfaces.Models;
using System.Collections.Generic;
using System.Linq;

namespace RickAndMortyDesktopApp.Modules.ModuleName.ViewModels
{
    public class CharacterViewModel
        : ViewModelBase
    {
        #region Properties

        private readonly ICharacterModel _characterModel;
        private readonly IEventAggregator _eventAggregator;

        public int Id => _characterModel.Id;
        public string Name => GetNonEmptyString(_characterModel.Name.ToUpper());
        public string Species => GetNonEmptyString(TrimName(_characterModel.Species));
        public string Status => GetNonEmptyString(TrimName(_characterModel.Status));
        public string Type => GetNonEmptyString(TrimName(_characterModel.Type));
        public string Gender => GetNonEmptyString(TrimName(_characterModel.Gender));
        public string Origin => GetNonEmptyString(TrimName(_characterModel.Origin.Name));
        public string OriginUrl => _characterModel.Origin.Url;
        public string Location => GetNonEmptyString(TrimName(_characterModel.Location.Name));
        public string LocationName => GetNonEmptyString(_characterModel.Location.Name);
        public string LocationUrl => _characterModel.Location.Url;
        public string Image => GetNonEmptyString(_characterModel.Image);
        public IList<string> Episodes => _characterModel.Episode
            .Select(e => GetNonEmptyString(TrimName(e.Name, 28)))
            .ToList();
        public IList<string> EpisodeUrls => _characterModel.Episode
            .Select(u => GetNonEmptyString(u.Url))
            .ToList();

        #endregion

        #region Initialize

        public CharacterViewModel(
            ICharacterModel model,
            IEventAggregator eventAggregator
            )
        {
            _characterModel = model;
            _eventAggregator = eventAggregator;

            DetailsCommand = new DelegateCommand(OnDetails);
        }

        #endregion

        #region Commands

        public DelegateCommand DetailsCommand { get; set; }
        public void OnDetails()
        {
            _eventAggregator
                .GetEvent<SelectedCharacterEvent>()
                .Publish(new SelectedCharacterEventArgs(Id));
        }

        #endregion

        #region Methods

        private static string GetNonEmptyString(string value)
        {
            return string.IsNullOrEmpty(value) ? "-" : value;
        }

        private string TrimName(string name, int maxLength = 20)
        {
            if (string.IsNullOrEmpty(name) || name.Length <= maxLength)
            {
                return name;
            }
            else
            {
                return name.Substring(0, maxLength) + "...";
            }
        }

        #endregion
    }
}
