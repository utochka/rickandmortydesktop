﻿using System.Collections.Generic;

namespace RickAndMortyDesktopApp.Services.Interfaces.DTOs.LocationDTOs
{
    public interface ILocationListDto
    {
        ILocationInfoDto Info { get; }
        IList<ILocationDto> Results { get; }
    }
}
