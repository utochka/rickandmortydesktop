﻿using Prism.Commands;
using Prism.Events;
using RickAndMortyDesktopApp.Core.Mvvm;
using RickAndMortyDesktopApp.Core.Views.Blackout;
using RickAndMortyDesktopApp.Core.Views.EpisodesView;
using RickAndMortyDesktopApp.Core.Views.LocationsView;
using RickAndMortyDesktopApp.Services.Interfaces;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace RickAndMortyDesktopApp.Modules.ModuleName.ViewModels
{
    public class EpisodesViewModel
        : ViewModelBase
    {
        #region Properties

        private IRickAndMortyApi _rickAndMortyApi;
        private IList<EpisodeViewModel> _models;
        private readonly IEventAggregator _eventAggregator;

        private int skip = 0;
        private const int take = 3;

        private EpisodeViewModel _selectedItem;
        public EpisodeViewModel SelectedItem
        {
            get => _selectedItem;
            set => SetProperty(ref _selectedItem, value);
        }

        #endregion

        #region Initialize

        public EpisodesViewModel(
            IRickAndMortyApi rickAndMortyApi,
            IEventAggregator eventAggregator
            )
        {
            _rickAndMortyApi = rickAndMortyApi;
            _eventAggregator = eventAggregator;

            _eventAggregator
                .GetEvent<SelectedEpisodeEvent>()
                .Subscribe(OnSelectedEpisode);

            NextCommand = new DelegateCommand(OnNext, CanExecuteNext);
            PreviousCommand = new DelegateCommand(OnPrevious, CanExecuteNext);
            ClosePopupCommand = new DelegateCommand(OnClosePopup);

            Task.Run(InitializeAsync);
        }

        private async Task InitializeAsync()
        {
            var models = await _rickAndMortyApi.GetEpisodesModelsAsync();
            _models = models
                .Select(a => new EpisodeViewModel(a, _eventAggregator))
                .ToList();

            List = new ObservableCollection<EpisodeViewModel>(_models.Skip(skip).Take(take));
        }

        #endregion

        #region Events
        private void OnSelectedEpisode(SelectedEpisodeEventArgs args)
        {
            SelectedItem = _models.Single(el => el.Id == args.Id);
            IsOpenPopup = true;

            _eventAggregator
               .GetEvent<BlackoutEvent>()
               .Publish(new BlackoutEventArgs(true));
        }

        public void OnClosePopup()
        {
            IsOpenPopup = false;
            SelectedItem = null;

            _eventAggregator
               .GetEvent<BlackoutEvent>()
               .Publish(new BlackoutEventArgs(false));
        }

        #endregion

        #region Commands

        public DelegateCommand PreviousCommand { set; get; }
        public DelegateCommand NextCommand { set; get; }

        private ObservableCollection<EpisodeViewModel> _list;
        public ObservableCollection<EpisodeViewModel> List
        {
            get => _list;
            set
            {
                if (SetProperty(ref _list, value))
                {
                    NextCommand.RaiseCanExecuteChanged();
                    PreviousCommand.RaiseCanExecuteChanged();
                }
            }
        }

        private void OnPrevious()
        {
            skip -= take;
            if (skip < 0)
            {
                skip = 0;
            }
            if (skip > _models.Count)
            {
                return;
            }

            List = new ObservableCollection<EpisodeViewModel>(_models.Skip(skip).Take(take));
        }

        private void OnNext()
        {
            skip += take;
            if (skip > _models.Count)
            {
                skip = _models.Count;
                return;
            }

            List = new ObservableCollection<EpisodeViewModel>(_models.Skip(skip).Take(take));
        }

        private bool CanExecuteNext()
        {
            if (_models == null)
            {
                return false;
            }
            return skip <= _models.Count;
        }

        public DelegateCommand ClosePopupCommand { set; get; }

        private bool _isOpenPopup;
        public bool IsOpenPopup
        {
            get => _isOpenPopup;
            set => SetProperty(ref _isOpenPopup, value);
        }

        #endregion
    }
}
