﻿using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;
using RickAndMortyDesktopApp.Core;
using RickAndMortyDesktopApp.Modules.ModuleName.Views;

namespace RickAndMortyDesktopApp.Modules.ModuleName
{
    public class ModuleNameModule : IModule
    {
        private readonly IRegionManager _regionManager;

        public ModuleNameModule(IRegionManager regionManager)
        {
            _regionManager = regionManager;
        }

        public void OnInitialized(IContainerProvider containerProvider)
        {
            _regionManager.RequestNavigate(RegionNames.ContentRegion, "CharactersView");
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<CharactersView>();
            containerRegistry.RegisterForNavigation<EpisodesView>();
            containerRegistry.RegisterForNavigation<LocationsView>();
            containerRegistry.RegisterForNavigation<ErrorView>();
        }
    }
}