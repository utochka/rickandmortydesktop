﻿using System.Collections.Generic;
using System;

namespace RickAndMortyDesktopApp.Services.Interfaces.DTOs.LocationDTOs
{
    public interface ILocationDto
    {
        int Id { get; }
        string Name { get; }
        string Type { get; }
        string Dimension { get; }
        List<string> Residents { get; }
        string Url { get; }
        DateTime Created { get; }
    }
}
