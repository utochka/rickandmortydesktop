﻿using System.Collections.Generic;
using System;
using RickAndMortyDesktopApp.Services.Interfaces.DTOs.CharacterDTOs;

namespace RickAndMortyDesktopApp.Services.DTOs.CharacterDTOs
{
    public class CharacterDto : ICharacterDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string Species { get; set; }
        public string Type { get; set; }
        public string Gender { get; set; }
        public OriginDto Origin { get; set; }
        public CharacterLocationDto Location { get; set; }
        public string Image { get; set; }
        public IList<string> Episode { get; set; }
        public string Url { get; set; }
        public DateTime Created { get; set; }

        IOriginDto ICharacterDto.Origin => Origin;

        ICharacterLocationDto ICharacterDto.Location => Location;
    }
}
