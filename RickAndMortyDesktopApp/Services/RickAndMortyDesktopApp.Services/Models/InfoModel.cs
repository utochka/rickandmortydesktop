﻿using RickAndMortyDesktopApp.Services.Interfaces.Models;

namespace RickAndMortyDesktopApp.Services.Models
{
    public class InfoModel : IInfoModel
    {
        public int Count { get; set; }
        public int Pages { get; set; }
        public string Next { get; set; }
        public string Prev { get; set; }
    }
}
