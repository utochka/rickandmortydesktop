﻿using System.Collections.Generic;
using System;

namespace RickAndMortyDesktopApp.Services.Interfaces.DTOs.EpisodeDTOs
{
    public interface IEpisodeDto
    {
        int Id { get; }
        string Name { get; }
        string Air_Date { get; }
        string Episode { get; }
        List<string> Characters { get; }
        string Url { get; }
        DateTime Created { get; }
    }
}
