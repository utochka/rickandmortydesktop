﻿using RickAndMortyDesktopApp.Services.Interfaces.DTOs.LocationDTOs;
using System;
using System.Collections.Generic;

namespace RickAndMortyDesktopApp.Services.DTOs.LocationDTOs
{
    public class LocationDto : ILocationDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Dimension { get; set; }
        public List<string> Residents { get; set; }
        public string Url { get; set; }
        public DateTime Created { get; set; }
    }
}
