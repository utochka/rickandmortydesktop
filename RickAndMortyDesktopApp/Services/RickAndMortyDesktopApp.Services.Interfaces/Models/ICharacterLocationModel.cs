﻿namespace RickAndMortyDesktopApp.Services.Interfaces.Models
{
    public interface ICharacterLocationModel
    {
        string Name { get; }
        string Url { get; }
    }
}
